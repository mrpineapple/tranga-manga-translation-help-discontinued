#!/usr/bin/env python
# -*- coding: utf8 -*-

import urllib,urllib2           #Web Access
from subprocess import call     #Calling External Processes
import time                     #Sleep
from xml.dom.minidom import parse, parseString  #XML parsing

#Rest of this is 'quickly' auto-generated stuff
import gettext
from gettext import gettext as _
gettext.textdomain('trangatranslate')
from gi.repository import Gtk # pylint: disable=E0611
import logging
logger = logging.getLogger('trangatranslate')
from trangatranslate_lib import Window
from trangatranslate.AboutTrangatranslateDialog import AboutTrangatranslateDialog
from trangatranslate.PreferencesTrangatranslateDialog import PreferencesTrangatranslateDialog
import os
from gi.repository import GLib

#Get stuff out of the returned xml
def parse_output(string):
    var = parseString(string)
    var = var.getElementsByTagName("translation")[0].toxml()
    data = var.replace('<translation>','').replace('</translation>','')
    return data

#Request a translation from the SysLang API
def getTranslate(url):
    a = call(["tesseract", url, "out" ,"-l" ,"jpn"])
    lines = [line.strip() for line in open("out.txt")]
    for a in range(len(lines)):
        #This was a major pain in the hole
        lines[a] = urllib.quote(lines[a])
        lines[a] = lines[a].encode('utf-8')
        lines[0] += lines[a]
    lines[0] = "http://syslang.com?src=ja&dest=en&text=" + lines[0] +"&email=tranga@gmail.com&password=tranga"
    #This API isn't great, but Google's one is paid and I couldn't find much else.
    headers = {
        #dunno, needed this to work. Thanks StackOverflow
        "User-Agent": "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:16.0) Gecko/20100101 Firefox/16.0"
    }
    try:
        req = urllib2.Request(lines[0], None, headers)
        get = urllib2.urlopen(req).read()
        return parse_output(get) 
    except (urllib2.HTTPError, urllib2.URLError), exception:
        print exception
        print lines[0]
        return "There was an error retrieving the translation. Try waiting 3 seconds for cooloff time"
       
       
# See trangatranslate_lib.Window.py for more details about how this class works
class TrangatranslateWindow(Window):
    __gtype_name__ = "TrangatranslateWindow"
    
    def finish_initializing(self, builder): # pylint: disable=E1002
        """Set up the main window"""
        super(TrangatranslateWindow, self).finish_initializing(builder)

        self.AboutDialog = AboutTrangatranslateDialog
        self.PreferencesDialog = PreferencesTrangatranslateDialog

        self.url = "/home/ian/Tranga/0006.jpg"

        # Code for other initialization actions should be added here.
        title = self.ui.image1.set_from_file(self.url)
            
    #When the "Translate" Button is pressed
    def on_button10_clicked(self,builder):
        self.ui.label2.set_text("Attempted Translation:\n"  + getTranslate(self.url))
